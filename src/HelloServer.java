import java.rmi.registry.*;
import java.rmi.server.*;
import javax.crypto.*;
import java.util.Scanner;

public class HelloServer{
	
	private static final String UNICODE_FORMAT = "UTF-8";
	
	public static void main(String args[]) {
		
		//important for the rmi registry location
		String ip = "0.0.0.0";
		System.setProperty("java.rmi.server.hostname","0.0.0.0");
		
		Registry reg = null;
		
		int port = 10900;
		try {
			//reg = LocateRegistry.getRegistry(ip, port);
			reg = LocateRegistry.createRegistry(port);
		}
		catch (Exception ce) {
			System.out.println();
			System.out.println("Error: "+ce.toString());
		}
		// creating instance of implemented class
		HelloImplementation hi = new HelloImplementation();
		try {
			Hello stub = (Hello) UnicastRemoteObject.exportObject(hi, 0);
			// bind token "Hello" and the created instance
			reg.rebind("Hello", stub);
			System.out.println("Server has been started.");
		}
		catch (Exception e) {
			System.out.println("Error: " + e.toString());
		}
		return;
	}
}
