import java.util.Scanner;

public class HelloImplementation implements Hello {
	
	private static String xor(String msg) {
		byte key[] = "2384ytgbhbgsvduig8712t3fiquwgbibkqwjfbihoubfvkjn".getBytes();
		byte msg_bytes[] = msg.getBytes();
		for (int i = 0; i < msg_bytes.length; ++i) {
			int key_idx = i % key.length;
			msg_bytes[i] ^= key[key_idx];
		}
		return new String(msg_bytes); 
	}
	
	public String chat(String msg)
	{
		System.out.println("Client> " + xor(msg));
		Scanner scan = new Scanner(System.in);
		String msg_sent = xor(scan.nextLine());
		System.out.println("Encoded message sent: " + msg_sent);
		return msg_sent;
	}
}
